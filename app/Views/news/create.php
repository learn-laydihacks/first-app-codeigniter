<h2> <?= esc($title); ?> </h2>

<!-- Informar los errores relacionados con la validacion de los formularios -->
<?= \Config\Services::validation()->listErrors(); ?> 


<form action="/news/create">
<label for="title">Title</label>
<input type="text" name="title">

<label for="body">Text</label>
<textarea name="body" ></textarea>

<input type="submit" name="submit" value="Create news item">
</form>