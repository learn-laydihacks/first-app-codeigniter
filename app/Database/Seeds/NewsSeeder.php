<?php

namespace App\Database\Seeds;

class NewsSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $data = array(
            [
                'id' => '1',
                'title'    => 'Elvis sighted',
                'slug' => 'elvis-sighted',
                'body' => 'Elvis was sighted at the Podunk internet cafe. It looked like he was writing a CodeIgniter app.',
            ],
            [
                'id' => '2',
                'title'    => 'Say it isn\'t so!',
                'slug' => 'say-it-isnt-so',
                'body' => 'Scientists conclude that some programmers have a sense of humor.',
            ],
            [
                'id' => '3',
                'title'    => 'Caffeination, Yes!',
                'slug' => 'caffeination-yes',
                'body' => 'World\'s largest coffee shop open onsite nested coffee shop for staff only.',
            ],
            
        );

        // Simple Queries
        // $query = "INSERT INTO news (id,title,slug,body) VALUES (:id:, :title:, :slug:, :body:)";
        // $this->db->query( $query,$data[0]);
    
        // Using Query Builder
        $this->db->table('news')->insert($data[0]);
        $this->db->table('news')->insert($data[1]);
        $this->db->table('news')->insert($data[2]);
    }
}

